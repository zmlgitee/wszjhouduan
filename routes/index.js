//引入express模块
const express=require('express');
const { RequestHeaderFieldsTooLarge } = require('http-errors');
//引入连接池模块
const pool=require('../pool.js');
//console.log(pool);
//创建路由器对象
const i=express.Router();
//首页轮播图
//接口地址:http://127.0.0.1:3000/index/rotation
//请求方法：get
// i.get('/rotation',(req,res)=>{
//   pool.query('select * from carousel',(err,result)=>{
//     if(err)throw err;
//     console.log(result)
//     res.send({
//       code:200,msg:'查询成功',data:result
//     })
//   })
// })
//查询购物车
//接口地址：http://127.0.0.1:3000/index/shop
//请求方法post
i.post('/shop',(req,res)=>{
  pool.query('select * from ws_shop where phone=?',[req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'成功',data:result
    })
  })
})
//添加购物车
//接口地址：http://127.0.0.1:3000/index/shop_insert
//请求方法post
i.post('/shop_insert',(req,res)=>{
  let obj=req.body
  pool.query('insert into ws_shop value(null,?,?,?,1,?,?)',[obj.title,obj.price,obj.pic,obj.color,obj.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'成功'
    })
  })
})
//修改购物车
//接口地址：http://127.0.0.1:3000/index/shop_num
//请求方法post
i.post('/shop_num',(req,res)=>{
  let obj=req.body
  pool.query('update ws_shop set shop_num=? where id=? && phone=?',[obj.num,obj.id,obj.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'成功'
    })
  })
})
//删除全部购物车订单
//接口地址：http://127.0.0.1:3000/index/shop_close_many
//请求方法post
i.post('/shop_close_many',(req,res)=>{
  for(var i=0;i<req.body.length;i++){
    pool.query(`delete from ws_shop where id=${req.body[i].id} && phone=${req.body[i].phone}`,(err,result)=>{
    if(err)throw err;
   }) 
  }
   res.send({
      code:200,msg:'成功'
    })
})
//删除购物车订单
//接口地址：http://127.0.0.1:3000/index/shop_close
//请求方法post
i.post('/shop_close',(req,res)=>{
  var obj=req.body
  pool.query('delete from ws_shop where id=? && phone=?',[obj.id,obj.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'成功'
    })
  })
})
//收件人信息
//接口地址：http://127.0.0.1:3000/index/address
//请求方法post
i.post('/address',(req,res)=>{
  pool.query('select * from ws_address where phone=?',[req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'成功',data:result
    })
  })
})
//增加收件人信息
//接口地址：http://127.0.0.1:3000/index/address_insert
//请求方法post
i.post('/address_insert',(req,res)=>{
  var obj=req.body
  pool.query('insert into ws_address values(null,?,?,?,?)',[obj.address_name,obj.address_phone,obj.address_address,obj.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'成功'
    })
  })
})
//删除收件人信息
//接口地址：http://127.0.0.1:3000/index/address_close
//请求方法post
i.post('/address_close',(req,res)=>{
  var obj=req.body
  pool.query('delete from ws_address where id=? && phone=?',[obj.id,obj.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'成功'
    })
  })
})
//用户订单信息
//接口地址：http://127.0.0.1:3000/index/order
//请求方法post
i.post('/order',(req,res)=>{
  pool.query('select * from ws_order where phone=? order by order_state asc ',[req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//全部订单信息
//接口地址：http://127.0.0.1:3000/index/whole_order
//请求方法post
i.post('/whole_order',(req,res)=>{
  pool.query('select * from ws_order order by order_state asc ',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//查询订单信息
//接口地址：http://127.0.0.1:3000/index/order_select
//请求方法post
i.post('/order_select',(req,res)=>{
  pool.query('select * from ws_order where id=? ',[req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//修改订单状态
//接口地址：http://127.0.0.1:3000/index/order_state
//请求方法post
i.post('/order_state',(req,res)=>{
  pool.query('update ws_order set order_state=? where id=?',[req.body.state,req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//删除订单信息
//接口地址：http://127.0.0.1:3000/index/order_close
//请求方法post
i.post('/order_close',(req,res)=>{
  pool.query('delete from ws_order where id=? && phone=?',[req.body.id,req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'成功'
    })
  })
})
//增加订单信息
//接口地址：http://127.0.0.1:3000/index/order_insert
//请求方法post
i.post('/order_insert',(req,res)=>{
  let o=req.body
  pool.query('insert into ws_order values(null,?,?,?,?,?,?,?,0,0,?,?)',[o.shop_title,o.shop_price,o.shop_pic,o.shop_num,o.address_name,o.address_phone,o.address_address,o.shop_color,o.phone],(err,result)=>{
    if(err)throw err; 
  }) 
  pool.query(`update ws_commodity set com_sales=(com_sales+${o.shop_num}) where com_title='${o.shop_title}'`,(err,result)=>{
    if(err)throw err
  })
  res.send({
      code:200,msg:'ok'
    })
})
//修改售后信息
//接口地址：http://127.0.0.1:3000/index/order_after
//请求方法post
i.post('/order_after',(req,res)=>{
  let obj=req.body
  pool.query('update ws_order set order_after=? where id=? && phone=?',[obj.order_after,obj.id,obj.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//后台修改售后信息
//接口地址：http://127.0.0.1:3000/index/admin_order_after
//请求方法post
i.post('/admin_order_after',(req,res)=>{
  let obj=req.body
  pool.query('update ws_order set order_after=? where id=?',[obj.order_after,obj.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//首页推荐商品信息
//接口地址：http://127.0.0.1:3000/index/recommend
//请求方法post
i.post('/recommend',(req,res)=>{
  pool.query('select * from ws_commodity order by com_sales desc limit 0,5',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//全部商品信息
//接口地址：http://127.0.0.1:3000/index/commodity_all
//请求方法post
i.post('/commodity_all',(req,res)=>{
  pool.query('select * from ws_commodity',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//商品信息
//接口地址：http://127.0.0.1:3000/index/commodity
//请求方法post
i.post('/commodity',(req,res)=>{
  pool.query('select * from ws_commodity where com_category=?',[req.body.cid],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//添加商品信息
//接口地址：http://127.0.0.1:3000/index/commodity_insert
//请求方法post
i.post('/commodity_insert',(req,res)=>{
  pool.query("insert into ws_commodity values(null,'','未定义标题','未定义副标题',0,0,0,'','官方套餐','','',10)",(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//修改商品信息
//接口地址：http://127.0.0.1:3000/commodity_update_pic
//请求方法post
i.post('/commodity_update_pic',(req,res)=>{
  pool.query("update ws_commodity set com_pic=? where id=?",[req.body.com_pic,req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//修改商品信息
//接口地址：http://127.0.0.1:3000/index/commodity_update
//请求方法post
i.post('/commodity_update',(req,res)=>{
  let o = req.body
  pool.query("update ws_commodity set com_title=?,com_introduction=?,com_oldprice=?,com_newprice=?,com_category=? where id=?",[o.com_title,o.com_introduction,o.com_oldprice,o.com_newprice,o.com_category,o.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//商品基本信息
//接口地址：http://127.0.0.1:3000/index/commodity_information
//请求方法post
i.post('/commodity_information',(req,res)=>{
  pool.query('select * from ws_commodity where id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//商品基本信息
//接口地址：http://127.0.0.1:3000/index/commodity_close
//请求方法post
i.post('/commodity_close',(req,res)=>{
  pool.query('delete from ws_commodity where id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
  })
  pool.query('delete from ws_features where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
  })
  pool.query('delete from ws_characteristic where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
  })
  pool.query('delete from ws_color where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
  })
  pool.query('delete from ws_commodity_rotation where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
  })
  pool.query('delete from ws_parameter where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
  })
  res.send({
      code:200,msg:'ok'
    })
})
//修改商品尺寸
//接口地址：http://127.0.0.1:3000/index/commodity_size_update
//请求方法post
i.post('/commodity_size_update',(req,res)=>{
  pool.query('update ws_commodity set com_size_1=?,com_size_2=? where id=?',[req.body.size1,req.body.size2,req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//商品颜色
//接口地址：http://127.0.0.1:3000/index/commodity_color
//请求方法post
i.post('/commodity_color',(req,res)=>{
  pool.query('select * from ws_color where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//添加商品颜色
//接口地址：http://127.0.0.1:3000/index/commodity_color_insert
//请求方法post
i.post('/commodity_color_insert',(req,res)=>{
  pool.query('insert into ws_color values(null,?,?)',[req.body.color,req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//商品特点
//接口地址：http://127.0.0.1:3000/index/commodity_characteristic
//请求方法post
i.post('/commodity_characteristic',(req,res)=>{
  pool.query('select * from ws_characteristic where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//增加商品特点
//接口地址：http://127.0.0.1:3000/index/commodity_characteristic_insert
//请求方法post
i.post('/commodity_characteristic_insert',(req,res)=>{
  pool.query("insert into ws_characteristic values(null,'',?,?)",[req.body.pic,req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//修改商品特点
//接口地址：http://127.0.0.1:3000/index/commodity_characteristic_insert_text
//请求方法post
i.post('/commodity_characteristic_insert_text',(req,res)=>{
  req.body.forEach(element => {
    pool.query("update ws_characteristic set chara_details=? where id=?",[element.chara_details,element.id],(err,result)=>{
    if(err)throw err;
    })
  });
    res.send({
      code:200,msg:'ok'
    })
})
//商品特征
//接口地址：http://127.0.0.1:3000/index/commodity_features
//请求方法post
i.post('/commodity_features',(req,res)=>{
  pool.query('select * from ws_features where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//增加商品特征
//接口地址：http://127.0.0.1:3000/index/commodity_features_insert
//请求方法post
i.post('/commodity_features_insert',(req,res)=>{
  pool.query("insert into ws_features values(null,'',?,'',?)",[req.body.pic,req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//修改商品特征
//接口地址：http://127.0.0.1:3000/index/commodity_features_update
//请求方法post
i.post('/commodity_features_update',(req,res)=>{
  req.body.forEach(item=>{
pool.query('update ws_features set feat_details=?,feat_title=? where id=?',[item.feat_details,item.feat_title,item.id],(err,result)=>{
    if(err)throw err;
  })
  })
  res.send({
      code:200,msg:'ok'
    })
})
//商品参数
//接口地址：http://127.0.0.1:3000/index/commodity_parameter
//请求方法post
i.post('/commodity_parameter',(req,res)=>{
  pool.query('select * from ws_parameter where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//增加商品参数
//接口地址：http://127.0.0.1:3000/index/commodity_parameter_insert
//请求方法post
i.post('/commodity_parameter_insert',(req,res)=>{
  pool.query('insert into ws_parameter values(null,?,?)',[req.body.text,req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//商品轮播图
//接口地址：http://127.0.0.1:3000/index/commodity_rotation
//请求方法post
i.post('/commodity_rotation',(req,res)=>{
  pool.query('select * from ws_commodity_rotation where com_id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//添加商品轮播图
//接口地址：http://127.0.0.1:3000/index/commodity_rotation_insert
//请求方法post
i.post('/commodity_rotation_insert',(req,res)=>{
  pool.query('insert into ws_commodity_rotation values(null,?,?)',[req.body.pic,req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//注册用户
//接口地址：http://127.0.0.1:3000/index/register
//请求方法post
i.post('/register',(req,res)=>{
  pool.query('insert into ws_user value(null,?,null,?,null,null,1)',[req.body.phone,req.body.pwd],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//查找手机
//接口地址：http://127.0.0.1:3000/index/phone
//请求方法post
i.post('/phone',(req,res)=>{
  pool.query('select * from ws_user where phone=?',[req.body.phone],(err,result)=>{
    if(err)throw err;
    if(result.length==0){
      res.send({
        code:201,msg:'未找到手机号码'
      })
    }else{
      res.send({
      code:200,msg:'ok'
    })
    }
  })
})
//用户登录
//接口地址：http://127.0.0.1:3000/index/login
//请求方法post
i.post('/login',(req,res)=>{
  pool.query('select * from ws_user where phone=? && user_pwd=?',[req.body.phone,req.body.pwd],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//修改用户信息
//接口地址：http://127.0.0.1:3000/index/setmessage
//请求方法post
i.post('/setmessage',(req,res)=>{
  pool.query('update ws_user set user_id=?,user_names=?,user_uname=?,user_sex=? where phone=?',[req.body.userid,req.body.names,req.body.uname,req.body.sex,req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//后台修改用户信息
//接口地址：http://127.0.0.1:3000/index/admin_set
//请求方法post
i.post('/admin_set',(req,res)=>{
  pool.query('update ws_user set user_id=?,user_names=?,user_uname=? where phone=?',[req.body.user_id,req.body.user_names,req.body.user_uname,req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//用户信息
//接口地址：http://127.0.0.1:3000/index/usermessage
//请求方法post
i.post('/usermessage',(req,res)=>{
  pool.query('select * from ws_user where phone=?',[req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//全部用户信息
//接口地址：http://127.0.0.1:3000/index/admin_user
//请求方法post
i.post('/admin_user',(req,res)=>{
  pool.query('select * from ws_user',[req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//删除用户信息
//接口地址：http://127.0.0.1:3000/index/admin_user_close
//请求方法post
i.post('/admin_user_close',(req,res)=>{
  pool.query('delete from ws_user where phone=?',[req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//修改密码
//接口地址：http://127.0.0.1:3000/index/setpwd
//请求方法post
i.post('/setpwd',(req,res)=>{
  pool.query('update ws_user set user_pwd=? where phone=? && user_pwd=?',[req.body.newpwd,req.body.phone,req.body.oldpwd],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//图赏
//接口地址：http://127.0.0.1:3000/index/picture
//请求方法post
i.post('/picture',(req,res)=>{
  pool.query('select * from ws_commodity_rotation',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//新闻
//接口地址：http://127.0.0.1:3000/index/news
//请求方法post
i.post('/news',(req,res)=>{
  pool.query('select * from ws_news',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//添加新闻
//接口地址：http://127.0.0.1:3000/index/news_insert
//请求方法post
i.post('/news_insert',(req,res)=>{
  pool.query('insert into ws_news values(null,?,?,?,?,?)',[req.body.title,req.body.date,req.body.person,req.body.content,req.body.href],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//首页轮播
//接口地址：http://127.0.0.1:3000/index/index_rotation
//请求方法post
i.post('/index_rotation',(req,res)=>{
  pool.query('select * from ws_index_rotation',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok',data:result
    })
  })
})
//删除首页轮播
//接口地址：http://127.0.0.1:3000/index/index_rotation_close
//请求方法post
i.post('/index_rotation_close',(req,res)=>{
  pool.query('delete from ws_index_rotation where id=?',[req.body.id],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
//添加首页轮播
//接口地址：http://127.0.0.1:3000/index/index_rotation_insert
//请求方法post
i.post('/index_rotation_insert',(req,res)=>{
  pool.query('insert into ws_index_rotation value(null,?,null)',[req.body.url],(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'ok'
    })
  })
})
module.exports=i;