var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// 加载CORS模块
const cors = require('cors');
// 使用CORS中间件
app.use(cors({
  origin: ['http://localhost:8080', 'http://127.0.0.1:8080']
}));

const fs = require("fs");
const multer = require("multer");
let objMulter = multer({ dest: "./public/images" }); 
//实例化multer，传递的参数对象，dest表示上传文件的存储路径
app.use(objMulter.any())//any表示任意类型的文件
// app.use(objMulter.image())//仅允许上传图片类型
app.post("/images", (req, res) => {
  let oldName = req.files[0].path;//获取名字
  //给新名字加上原来的后缀
  let newName = req.files[0].path + path.parse(req.files[0].originalname).ext;
  fs.renameSync(oldName, newName);//改图片的名字
  res.send({
    err: 0,
    url:
      req.files[0].filename +
      path.parse(req.files[0].originalname).ext//该图片的预览路径
  });
});
 

app.get('/public/images/*', function (req, res) {
  res.sendFile( __dirname + "/" + req.url );
  console.log("Request for " + req.url + " received.");
})

app.post('/close',(req,res)=>{
  fs.unlink(`./public/images/${req.body.url}`, function(err) {
    if (err) {
        return console.error(err);
    }
    res.send({
      code:200,
      msg:'删除成功'
    })
 });
})

app.use('/index', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
